﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;

using QSuper.SSE.Services.Importservice.DataHolders;

using Serilog;

namespace QSuper.SSE.Services.Importservice.Helpers
{
    class Commons
    {
        private static OrganizationService service;
        private static EntityReference entityRefOwnerTeam;
        private static double activityResponseTimeDays = -1;

        # region GetOrganizationService
        public static OrganizationService GetOrganizationService()
        {
            if (service == null)
            {
                CrmConnection connection = new CrmConnection("Xrm");
                service = new OrganizationService(connection);
            }

            return service;
        }
        # endregion

        # region GetEmployerReference Method
        // Get Employer entity reference if employer already exists.
        // If employer does not exist in CRM, create a new Employer with given Accurity Id and Employer name
        public static EntityReference GetEmployerReference(string employerAccurityId, string employerName)
        {
            EntityReference employerRef = null;
            QueryExpression query = new QueryExpression(Constants.ENTITYNAME_EMPLOYER);
            ConditionExpression condition = new ConditionExpression(Constants.ATTRIBUTENAME_EMPLOYER_ACCURITYID, ConditionOperator.Equal, employerAccurityId);
            query.Criteria.Conditions.Add(condition);
            query.ColumnSet = new ColumnSet(true);
            EntityCollection employers = GetOrganizationService().RetrieveMultiple(query);

            if (employers.Entities.Count > 0)
            {
                employerRef = new EntityReference(employers.Entities[0].LogicalName, employers.Entities[0].Id);
            }
            else
            {
                return CreateHelper.CreateEmployerEntity(employerAccurityId, employerName);
            }
            return employerRef;
        }
        # endregion

        # region GetCustomizationValue Method
        // Connect to CRM and get customizations entity value for the key
        public static string GetCustomizationValue(string key)
        {
            string value = null;

            QueryExpression query = new QueryExpression("new_ssecustomizations");
            ConditionExpression condition = new ConditionExpression("new_name", ConditionOperator.Equal, key);
            query.Criteria.Conditions.Add(condition);
            query.ColumnSet = new ColumnSet(true);
            EntityCollection customizations = GetOrganizationService().RetrieveMultiple(query);

            if (customizations.Entities.Count > 0)
            {
                value = customizations.Entities[0]["new_value"].ToString();
            }
            return value;
        }
        # endregion

        # region GetOwnerTeam Method
        public static EntityReference GetOwnerTeam()
        {
            if (entityRefOwnerTeam == null)
            {
                string ownerTeamName = GetCustomizationValue(Constants.CUSTOMIZATION_OWNERTEAM);
                QueryExpression query = new QueryExpression("team");
                ConditionExpression condition = new ConditionExpression("name", ConditionOperator.Equal, ownerTeamName);
                query.Criteria.Conditions.Add(condition);
                query.ColumnSet = new ColumnSet(true);
                EntityCollection teams = GetOrganizationService().RetrieveMultiple(query);

                if (teams.Entities.Count > 0)
                {
                    entityRefOwnerTeam = new EntityReference(teams.Entities[0].LogicalName, teams.Entities[0].Id);
                }
            }

            return entityRefOwnerTeam;
        }
        # endregion

        # region GetActivityResponseDays Method
        public static double GetActivityResponseDays()
        {
            if (activityResponseTimeDays == -1)
            {
                activityResponseTimeDays = double.Parse(Commons.GetCustomizationValue(Constants.CUSTOMIZATION_RESPONSETIME));
            }

            return activityResponseTimeDays;
        }
        # endregion        

        # region MoveFileMethod
        public static void MoveFile(string sourceFilePath, string destinationPath)
        {
            string destinationFullPath = destinationPath + "/" + Path.GetFileName(sourceFilePath);

            // If directory does not exists, create the Completed Directory structure
            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }

            // If file already exists, delete the old file and then move the file.
            if (File.Exists(destinationFullPath))
            {
                File.Delete(destinationFullPath);
            }

            File.Move(sourceFilePath, destinationFullPath);
        }
        #endregion

        # region MoveFilesMethod
        public static void MoveFiles(string sourceDir, string destinationDir)
        {
            string destinationFullPath; 

            // If directory does not exists, create the Completed Directory structure
            if (!Directory.Exists(destinationDir))
            {
                Directory.CreateDirectory(destinationDir);
            }

            foreach (var file in Directory.GetFiles(sourceDir))
            {
                destinationFullPath = destinationDir + "/" + Path.GetFileName(file);
                // If file already exists, delete the old file and then move the file.
                if (File.Exists(destinationFullPath))
                {
                    File.Delete(destinationFullPath);
                }

                File.Move(file, destinationFullPath);
            }
            
        }
        #endregion

        # region MandatoryFieldCheck
        public static string MandatoryFieldCheckString(string field, string fieldName)
        {
            if (field == null || field.Length == 0)
            {
                throw new Exception(fieldName + " cannot be empty");
            }

            return field;
        }
        # endregion

        # region GetNoOfErrorsForWorkbenchFile
        public static int GetNoOfErrorsForWorkbenchFile(WorkbenchFile objWorkbenchFile)
        {
            int totalErrors = 0;
            foreach (Employer employer in objWorkbenchFile.Employers)
            {
                totalErrors = totalErrors + employer.Errors.Count;
            }
            return totalErrors;
        }
        # endregion

    }
}
