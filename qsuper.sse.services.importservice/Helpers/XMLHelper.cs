﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

using Serilog;

using QSuper.SSE.Services.Importservice.DataHolders;

namespace QSuper.SSE.Services.Importservice.Helpers
{
    class XMLHelper
    {
        # region GetAllWorkbenchFileObjectsFromPath
        public static List<WorkbenchFile> GetAllWorkbenchFileObjectsFromPath(string filePath)
        {
            List<WorkbenchFile> workbenchFiles = new List<WorkbenchFile>();
            WorkbenchFile tempWorkbenchFile;

            // Read all files in the file path provided
            string[] xmlFiles = Directory.GetFiles(filePath);

            // If no file is found for parsing, log and return
            if (xmlFiles.Count() == 0)
            {
                Log.Debug("No files found for Parsing");
                return workbenchFiles;
            }

            // Parse XML and get file data(for each file)
            foreach (string xmlFile in xmlFiles)
            {
                Log.Debug("------------------------------------------------");
                Log.Debug("Parsing and converting " + xmlFile +" to Object");
                tempWorkbenchFile = ConvertXMLToObject(xmlFile);
                if (tempWorkbenchFile != null)
                {
                    workbenchFiles.Add(tempWorkbenchFile);
                }
            }

            return workbenchFiles;
        }
        # endregion

        # region ConvertXMLToObject
        public static WorkbenchFile ConvertXMLToObject(string xmlFile)
        {
            try
            {
                // XML Variables
                XmlDocument xmlDoc;
                XmlNodeList employersNodeList;

                // Data holding Variables
                WorkbenchFile workbenchFile = new WorkbenchFile();
                Employer employer;
                Error error;
                List<Employer> employers;
                List<Error> errors;

                xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlFile);

                // Get the File Reference No.
                workbenchFile.FileReferenceNo = Commons.MandatoryFieldCheckString(xmlDoc.DocumentElement.SelectSingleNode(Constants.XMLPATH_FROMROOT_FILEREFNO).InnerText, "File Reference Number");
                workbenchFile.FileName = Path.GetFileName(xmlFile);
                workbenchFile.FileFullPath = xmlFile;                
                Log.Debug("File Reference No : " + workbenchFile.FileReferenceNo);

                // Get All Employer Nodes
                employersNodeList = xmlDoc.DocumentElement.SelectNodes(Constants.XMLPATH_FROMROOT_EMPLOYERS);

                // Get Each Employer details
                employers = new List<Employer>();
                foreach (XmlNode employerNode in employersNodeList)
                {
                    employer = new Employer();
                    employer.EmployerId = Commons.MandatoryFieldCheckString(employerNode.SelectSingleNode(Constants.XMLPATH_FROMEMPLOYER_EMPLOYERID).InnerText, "Employer Id");
                    employer.EmployerName = Commons.MandatoryFieldCheckString(employerNode.SelectSingleNode(Constants.XMLPATH_FROMEMPLOYER_EMPLOYERNAME).InnerText, "Employer Name");
                    Log.Debug("Employer Id : " + employer.EmployerId + ", Name : " + employer.EmployerName);

                    // Get All Errors for this Employer
                    XmlNodeList errorNodeList = employerNode.SelectNodes(Constants.XMLPATH_FROMEMPLOYER_EXCEPTIONS);

                    errors = new List<Error>();
                    foreach (XmlNode errorNode in errorNodeList)
                    {
                        error = new Error();
                        error.Subject = Commons.MandatoryFieldCheckString(errorNode.SelectSingleNode(Constants.XMLPATH_FROMEXCEPTIONS_SUBJECT).InnerText, "Error Subject");
                        Log.Debug("Error Subject : " + error.Subject);
                        error.MessageCode = errorNode.SelectSingleNode(Constants.XMLPATH_FROMEXCEPTIONS_MESSAGECODE).InnerText;
                        error.SequenceNumber = errorNode.SelectSingleNode(Constants.XMLPATH_FROMEXCEPTIONS_SEQUENCENO).InnerText;
                        error.FileLineNumber = errorNode.SelectSingleNode(Constants.XMLPATH_FROMEXCEPTIONS_FILELINENO).InnerText;
                        error.ClientId = Commons.MandatoryFieldCheckString(errorNode.SelectSingleNode(Constants.XMLPATH_FROMEXCEPTIONS_CLIENTID).InnerText, "Error Client Id");
                        error.AcurityExceptionId = Commons.MandatoryFieldCheckString(errorNode.SelectSingleNode(Constants.XMLPATH_FROMEXCEPTIONS_ACCURITYEXCEPTIONID).InnerText, "Accurity Exception Id");

                        errors.Add(error);
                    }
                    employer.Errors = errors;
                    employers.Add(employer);
                }
                workbenchFile.Employers = employers;
                return workbenchFile;
            }
            catch (Exception e)
            {
                // Move File to failed Folder
                Commons.MoveFile(xmlFile, Directory.GetParent(Path.GetDirectoryName(xmlFile)) + "/" + Constants.FOLDERNAME_FAILED);
                Console.WriteLine("Error - " + e.Message + ", " + e.InnerException);
                Log.Error("Error occured while getting data from XML file : " + xmlFile);
                Log.Error("Error : " + e.Message + ", " + e.InnerException);
                return null;
            }
        }
        # endregion

    }
}
