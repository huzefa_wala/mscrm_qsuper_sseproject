﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;

using QSuper.SSE.Services.Importservice.DataHolders;

using Serilog;

namespace QSuper.SSE.Services.Importservice.Helpers
{
    class UpdateHelper
    {
        # region UpdateWorkbenchFileEntity Method
        // Update the workbenchfile entity
        public static void UpdateWorkbenchFileEntity(WorkbenchFile objWorkbenchFile)
        {
            List<Error> errors;
            try
            {
                // Get the existing workbench file entity with the File Reference no.
                QueryExpression fileQuery = new QueryExpression(Constants.ENTITYNAME_WORKBENCHFILE);
                ConditionExpression fileCondition1 = new ConditionExpression(Constants.ATTRIBUTENAME_WORKBENCHFILE_REFERENCENO, ConditionOperator.Equal, objWorkbenchFile.FileReferenceNo);
                fileQuery.Criteria.AddCondition(fileCondition1);
                fileQuery.ColumnSet = new ColumnSet(true);

                EntityCollection workbenchFiles = Commons.GetOrganizationService().RetrieveMultiple(fileQuery);
                if (workbenchFiles.Entities.Count > 0)
                {
                    // Get all cases for the workbench file
                    QueryExpression caseQuery = new QueryExpression(Constants.ENTITYNAME_INCIDENT);
                    ConditionExpression caseCondition1 = new ConditionExpression(Constants.ATTRIBUTENAME_INCIDENT_FILE, ConditionOperator.Equal, workbenchFiles[0].Id);
                    caseQuery.Criteria.AddCondition(caseCondition1);
                    caseQuery.ColumnSet = new ColumnSet(true);

                    EntityCollection cases = Commons.GetOrganizationService().RetrieveMultiple(caseQuery);
                    if (cases.Entities.Count > 0)
                    {
                        // for each case
                        foreach (Entity entityCase in cases.Entities)
                        {
                            // Get all errors from update xml for the employer related to this case
                            errors = GetListOfErrorsForCase(entityCase, objWorkbenchFile);

                            // Check which errors are missing and close missing errors
                            UpdateActivitiesForCase(entityCase, errors);

                            // for each case, check which errors are new, add new activities
                            foreach (Error error in errors)                        
                            {
                                CheckMissingErrorAndCreate(error, entityCase);
                            }   
                        }                    

                        // Log Success   
                        Commons.MoveFile(objWorkbenchFile.FileFullPath, Directory.GetParent(Path.GetDirectoryName(objWorkbenchFile.FileFullPath)) + "/" + Constants.FOLDERNAME_COMPLETED);
                    }
                    else
                    {
                        throw new Exception("No cases found for file Reference No. : " + objWorkbenchFile.FileReferenceNo);
                    }
                    
                }
                else
                {
                    throw new Exception("No Entity found with File Reference No. : " + objWorkbenchFile.FileReferenceNo);
                }
            }
            catch (Exception e)
            {
                Log.Error("Error occured while updating Entities.");
                Log.Error("Error is : " + e.Message + ", " + e.InnerException);
                                
                // Move file to Failed folder
                Commons.MoveFile(objWorkbenchFile.FileFullPath, Directory.GetParent(Path.GetDirectoryName(objWorkbenchFile.FileFullPath)) + "/" + Constants.FOLDERNAME_FAILED);
            }
        }
        # endregion

        # region UpdateActivitiesForCase
        // Checks which errors are missing for the case (employer), in the update XML and marks them as completed in CRM
        private static void UpdateActivitiesForCase(Entity entityCase, List<Error> errorsInFile){
            // Get all Activities for the case
            QueryExpression query = new QueryExpression(Constants.ENTITYNAME_WORKBENCHACTIVITY);
            ConditionExpression condition1 = new ConditionExpression(Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CASE, ConditionOperator.Equal, entityCase.Id);
            query.Criteria.AddCondition(condition1);
            query.ColumnSet = new ColumnSet(true);

            EntityCollection workbenchActivities = Commons.GetOrganizationService().RetrieveMultiple(query);
            if (workbenchActivities.Entities.Count > 0)
            {         
                // close missing errors
                foreach (Entity workbenchActivity in workbenchActivities.Entities)
                {
                    CheckMissingActivitiesAndUpdateStatus(errorsInFile, workbenchActivity);
                }
            }
        }
        # endregion

        # region CheckMissingActivityAndUpdateStatus
        // Checks if the crmworkbenchactivity is available in the file errors. If not, mark the activity as completed
        private static void CheckMissingActivitiesAndUpdateStatus(List<Error> errorsInFile, Entity crmWorkbenchActivityEntity)
        {
            foreach (Error error in errorsInFile)
            {
                // Compare AccurityExceptionId and Client Id of WorkbenchActivity with all errors in file
                if (error.ClientId.Equals(crmWorkbenchActivityEntity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CLIENTID]) && 
                    error.AcurityExceptionId.Equals(crmWorkbenchActivityEntity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_ACCURITYEXCEPTIONID])){
                    return;
                }
            }

            // CRM workbenchactivity is not in file, means its status is closed in Accurity
            // Close status in CRM
            UpdateWorkbenchActivityStatusToClosed(crmWorkbenchActivityEntity);
        }
        # endregion

        # region UpdateWorkbenchActivityStatusToClosed Method
        // Mark the specific work bench activity as Closed
        private static void UpdateWorkbenchActivityStatusToClosed(Entity workbenchActivity)
        {
            // Create the Request Object
            SetStateRequest state = new SetStateRequest();
            state.State = new OptionSetValue(1);
            state.Status = new OptionSetValue(2);
            state.EntityMoniker = new EntityReference(workbenchActivity.LogicalName, workbenchActivity.Id);
            Commons.GetOrganizationService().Execute(state);

            Log.Debug("Workbench Activity closed for AccurityException Id : " + workbenchActivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_ACCURITYEXCEPTIONID] + 
                " and client id : " + workbenchActivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CLIENTID]);
        }
        # endregion

        # region CheckMissingErrorAndCreate
        // Check errors which are in the update xml file and not in CRM, and add them to crm under the case
        private static void CheckMissingErrorAndCreate(Error error, Entity entityCase)
        {
            // Check if error is present in CRM, for the specific case
            QueryExpression query = new QueryExpression(Constants.ENTITYNAME_WORKBENCHACTIVITY);
            ConditionExpression condition1 = new ConditionExpression(Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CASE, ConditionOperator.Equal, entityCase.Id);
            query.Criteria.AddCondition(condition1);
            ConditionExpression condition2 = new ConditionExpression(Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CLIENTID, ConditionOperator.Equal, error.ClientId);
            query.Criteria.AddCondition(condition2);
            ConditionExpression condition3 = new ConditionExpression(Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_ACCURITYEXCEPTIONID, ConditionOperator.Equal, error.AcurityExceptionId);
            query.Criteria.AddCondition(condition3);
            query.ColumnSet = new ColumnSet(true);

            EntityCollection workbenchActivities = Commons.GetOrganizationService().RetrieveMultiple(query);
            if (workbenchActivities.Entities.Count == 0)
            {
                // If not, create new WorkbenchActivity for the error
                CreateHelper.CreateWorkbenchActivityEntity(null, error, new EntityReference(entityCase.LogicalName, entityCase.Id));
            }
        }
        # endregion

        # region GetListOfErrorsForCase
        /* Gets the employer from the case, searches the update file for the employer and returns all errors for the employer in the update xml */
        private static List<Error> GetListOfErrorsForCase(Entity entityCase, WorkbenchFile objWorkbenchFile)
        {
            // Get the employer from the Case
            EntityReference entityRefEmployer = (EntityReference)entityCase[Constants.ATTRIBUTENAME_INCIDENT_EMPLOYER];
            Entity entityEmployer = Commons.GetOrganizationService().Retrieve(entityRefEmployer.LogicalName, entityRefEmployer.Id, new ColumnSet(true));

            // for each employer
            foreach (Employer employer in objWorkbenchFile.Employers)
            {
                // Compare employer from case to employer from file
                if (employer.EmployerId.Equals(entityEmployer[Constants.ATTRIBUTENAME_EMPLOYER_ACCURITYID]))
                {
                    // If same, return Errors read for the employer from the update file
                    return employer.Errors;
                }
            }

            // No employer found, return empty list
            return new List<Error>();
        }
        # endregion
    }
}
