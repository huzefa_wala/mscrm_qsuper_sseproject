﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;

using QSuper.SSE.Services.Importservice.DataHolders;

using Serilog;

namespace QSuper.SSE.Services.Importservice.Helpers
{
    class CreateHelper
    {
        # region CreateWorkbenchFileEntity Method
        // Create a new WorkbenchFile Entity from the given data
        public static void CreateWorkbenchFileEntity(WorkbenchFile objWorkbenchFile)
        {
            List<Entity> transactionEntities = new List<Entity>();
            try
            {                
                EntityReference entityRefWorkbenchFile;

                Entity workbenchFile = new Entity(Constants.ENTITYNAME_WORKBENCHFILE);
                workbenchFile[Constants.ATTRIBUTENAME_WORKBENCHFILE_FILENAME] = objWorkbenchFile.FileName;
                workbenchFile[Constants.ATTRIBUTENAME_WORKBENCHFILE_REFERENCENO] = objWorkbenchFile.FileReferenceNo;                
                workbenchFile[Constants.ATTRIBUTENAME_WORKBENCHFILE_IMPORTDATE] = DateTime.UtcNow;
                workbenchFile[Constants.ATTRIBUTENAME_WORKBENCHFILE_NOOFEMPLOYER] = objWorkbenchFile.Employers.Count;
                workbenchFile[Constants.ATTRIBUTENAME_WORKBENCHFILE_NOOFERRORS] = Commons.GetNoOfErrorsForWorkbenchFile(objWorkbenchFile);
                workbenchFile[Constants.ATTRIBUTENAME_WORKBENCHFILE_OWNER] = Commons.GetOwnerTeam();

                workbenchFile.Id = Commons.GetOrganizationService().Create(workbenchFile);
                transactionEntities.Add(workbenchFile);
                Log.Debug("Workbench File Created : " + objWorkbenchFile.FileReferenceNo);
                entityRefWorkbenchFile = new EntityReference(workbenchFile.LogicalName, workbenchFile.Id);

                foreach (Employer employer in objWorkbenchFile.Employers)
                {
                    CreateCaseEntity(transactionEntities, employer, entityRefWorkbenchFile, objWorkbenchFile.FileReferenceNo);
                }

                // Log Success   
                Commons.MoveFile(objWorkbenchFile.FileFullPath, Directory.GetParent(Path.GetDirectoryName(objWorkbenchFile.FileFullPath)) + "/" + Constants.FOLDERNAME_COMPLETED);
            }
            catch (Exception e)
            {
                Log.Error("Error occured while creating Entities. Deleting all entities created for this file");
                Log.Error("Error is : " + e.Message + ", " + e.InnerException);

                // If there is an error while creating any entity (file, case or activity), delete all entities created for this file.
                foreach (Entity entity in transactionEntities)
                {
                    try
                    {
                        Commons.GetOrganizationService().Delete(entity.LogicalName, entity.Id);
                    }
                    catch (Exception)
                    {
                        // Those entities which were in Execution pipeline were already deleted by CRM.
                        // If those entities cannot be found, exception will be thrown. Ignore them.
                    }
                }
                // Move file to Failed folder
                Commons.MoveFile(objWorkbenchFile.FileFullPath, Directory.GetParent(Path.GetDirectoryName(objWorkbenchFile.FileFullPath)) + "/" + Constants.FOLDERNAME_FAILED);
            }
        }
        # endregion

        # region CreateCaseEntity Method
        // Create a new WorkbenchFile Entity from the given data
        public static void CreateCaseEntity(List<Entity> transactionEntities, Employer employer, EntityReference entityRefWorkbenchFile, string fileRefNo)
        {
            EntityReference entityRefEmployer = Commons.GetEmployerReference(employer.EmployerId, employer.EmployerName);
            EntityReference entityRefCase;

            Entity caseEntity = new Entity(Constants.ENTITYNAME_INCIDENT);
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_TITLE] = "Workbench Error(s) for " + employer.EmployerName + " - " + fileRefNo;
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_EMPLOYER] = entityRefEmployer;
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_FILE] = entityRefWorkbenchFile;
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_CASETYPE] = new OptionSetValue(100000000);
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_CASEORIGIN] = new OptionSetValue(100000000);
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_OWNER] = Commons.GetOwnerTeam(); ;
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_WORKBENCHBREACH] = new OptionSetValue(100000000);
            caseEntity[Constants.ATTRIBUTENAME_INCIDENT_NOOFERRORS] = employer.Errors.Count;
            // Priority automatically set by plugin which runs on Create / Update of Workbench activities

            caseEntity.Id = Commons.GetOrganizationService().Create(caseEntity);
            transactionEntities.Add(caseEntity);
            Log.Debug("Case Created for Employer : " + employer.EmployerName);
            entityRefCase = new EntityReference(caseEntity.LogicalName, caseEntity.Id);

            foreach (Error error in employer.Errors)
            {
                CreateWorkbenchActivityEntity(transactionEntities, error, entityRefCase);
            }
        }
        # endregion

        # region CreateWorkbenchActivityEntity Method
        // Create a new WorkbenchFile Entity from the given data
        public static void CreateWorkbenchActivityEntity(List<Entity> transactionEntities, Error error, EntityReference caseEntityRef)
        {
            Entity workbenchactivity = new Entity(Constants.ENTITYNAME_WORKBENCHACTIVITY);
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_SUBJECT] = error.Subject;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_MESSAGECODE] = error.MessageCode;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_SEQUENCENUMBER] = error.SequenceNumber;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CLIENTID] = error.ClientId;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_ACCURITYEXCEPTIONID] = error.AcurityExceptionId;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_FILELINENO] = error.FileLineNumber;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_OWNER] = Commons.GetOwnerTeam();
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_CASE] = caseEntityRef;
            workbenchactivity[Constants.ATTRIBUTENAME_WORKBENCHACTIVITY_DUEDATE] = DateTime.Now.AddDays(Commons.GetActivityResponseDays());            

            workbenchactivity.Id = Commons.GetOrganizationService().Create(workbenchactivity);
            if (transactionEntities != null)
            {
                transactionEntities.Add(workbenchactivity);
            }
            
            Log.Debug("Workbench Activity Created with subject : " + error.Subject);
        }
        # endregion

        # region CreateEmployerEntity Method
        // Create a new Employer Entity from the given data
        public static EntityReference CreateEmployerEntity(string accurityId, string employerName)
        {
            Log.Debug("Employer '" + employerName + "' not present in CRM. Creating new record for employer");
            Entity employer = new Entity(Constants.ENTITYNAME_EMPLOYER);
            employer[Constants.ATTRIBUTENAME_EMPLOYER_ACCURITYID] = accurityId;
            employer[Constants.ATTRIBUTENAME_EMPLOYER_NAME] = employerName;

            Guid Id = Commons.GetOrganizationService().Create(employer);

            return new EntityReference(Constants.ENTITYNAME_EMPLOYER, Id);
        }
        # endregion
    }
}
