﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QSuper.SSE.Services.Importservice.Helpers
{
    class Constants
    {
        public const string CUSTOMIZATION_FILEPATH_NEW = "XML New File Path";
        public const string CUSTOMIZATION_FILEPATH_UPDATE = "XML Update File Path";
        public const string CUSTOMIZATION_OWNERTEAM = "Case Owner Team";
        public const string CUSTOMIZATION_RESPONSETIME = "Activity Response Time";

        public const string XMLPATH_FROMROOT_FILEREFNO = "/WorkbenchFile/FileReferenceNo";
        public const string XMLPATH_FROMROOT_EMPLOYERS = "/WorkbenchFile/Employers/Employer";
        public const string XMLPATH_FROMEMPLOYER_EMPLOYERID = "EmployerId";
        public const string XMLPATH_FROMEMPLOYER_EMPLOYERNAME = "EmployerName";
        public const string XMLPATH_FROMEMPLOYER_EXCEPTIONS = "Exceptions/Exception";
        public const string XMLPATH_FROMEXCEPTIONS_SUBJECT = "Subject";
        public const string XMLPATH_FROMEXCEPTIONS_MESSAGECODE = "MessageCode";
        public const string XMLPATH_FROMEXCEPTIONS_SEQUENCENO = "SequenceNo";
        public const string XMLPATH_FROMEXCEPTIONS_FILELINENO = "FileLineNo";
        public const string XMLPATH_FROMEXCEPTIONS_CLIENTID = "ClientId";
        public const string XMLPATH_FROMEXCEPTIONS_ACCURITYEXCEPTIONID = "AcurityExceptionId";

        public const string ENTITYNAME_EMPLOYER = "account";
        public const string ENTITYNAME_WORKBENCHFILE = "new_workbenchfiles";
        public const string ENTITYNAME_INCIDENT = "incident";
        public const string ENTITYNAME_WORKBENCHACTIVITY = "new_workbenchactivity";

        public const string ATTRIBUTENAME_EMPLOYER_ACCURITYID = "qinvest_employerid";
        public const string ATTRIBUTENAME_EMPLOYER_NAME = "name";
        public const string ATTRIBUTENAME_WORKBENCHFILE_FILENAME = "new_filename";
        public const string ATTRIBUTENAME_WORKBENCHFILE_REFERENCENO = "new_referenceno";        
        public const string ATTRIBUTENAME_WORKBENCHFILE_IMPORTDATE = "new_importdate";
        public const string ATTRIBUTENAME_WORKBENCHFILE_NOOFEMPLOYER = "new_noofemployers";
        public const string ATTRIBUTENAME_WORKBENCHFILE_NOOFERRORS = "new_nooferrors";
        public const string ATTRIBUTENAME_WORKBENCHFILE_OWNER = "ownerid";
        public const string ATTRIBUTENAME_INCIDENT_TITLE = "title";
        public const string ATTRIBUTENAME_INCIDENT_EMPLOYER = "customerid";
        public const string ATTRIBUTENAME_INCIDENT_FILE = "new_file";
        public const string ATTRIBUTENAME_INCIDENT_CASETYPE = "casetypecode";
        public const string ATTRIBUTENAME_INCIDENT_CASEORIGIN = "caseorigincode";
        public const string ATTRIBUTENAME_INCIDENT_OWNER = "ownerid";
        public const string ATTRIBUTENAME_INCIDENT_WORKBENCHBREACH = "new_workbenchbreach";
        public const string ATTRIBUTENAME_INCIDENT_NOOFERRORS = "new_nooferrors";        
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_SUBJECT = "subject";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_MESSAGECODE = "new_messagecode";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_SEQUENCENUMBER = "new_sequencenumber";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_CLIENTID = "new_clientid";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_FILELINENO = "new_filelineno";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_OWNER = "ownerid";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_CASE = "new_case";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_DUEDATE = "scheduledend";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_REGARDING = "regardingobjectid";
        public const string ATTRIBUTENAME_WORKBENCHACTIVITY_ACCURITYEXCEPTIONID = "new_accurityexceptionid";

        public const string FOLDERNAME_COMPLETED = "Completed";
        public const string FOLDERNAME_PROCESSING = "Processing";
        public const string FOLDERNAME_FAILED = "Failed";
    }
}
