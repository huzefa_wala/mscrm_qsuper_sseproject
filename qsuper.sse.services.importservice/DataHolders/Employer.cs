﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QSuper.SSE.Services.Importservice.DataHolders
{
    class Employer
    {
        public string EmployerId { get; set; }
        public string EmployerName { get; set; }
        public List<Error> Errors { get; set; }
    }
}
