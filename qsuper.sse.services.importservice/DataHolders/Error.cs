﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QSuper.SSE.Services.Importservice.DataHolders
{
    class Error
    {
        public string AcurityExceptionId {get; set;}
        public string Subject { get; set; }
        public string MessageCode { get; set; }
        public string SequenceNumber { get; set; }
        public string FileLineNumber { get; set; }
        public string ClientId { get; set; }
    }
}
