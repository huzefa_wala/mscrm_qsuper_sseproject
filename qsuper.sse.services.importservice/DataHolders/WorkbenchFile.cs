﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QSuper.SSE.Services.Importservice.DataHolders
{
    class WorkbenchFile
    {
        public string FileName { get; set; }
        public string FileFullPath { get; set; }
        public string FileReferenceNo { get; set; }
        public List<Employer> Employers { get; set; }
    }
}
