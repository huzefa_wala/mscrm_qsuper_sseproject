﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Configuration;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;

using QSuper.SSE.Services.Importservice.DataHolders;
using QSuper.SSE.Services.Importservice.Helpers;

using Serilog;

namespace QSuper.SSE.Services.Importservice
{
    class Program
    {
        # region Main Method
        static void Main(string[] args)
        {
            try
            {
                string logFileLocationNew = ConfigurationManager.AppSettings["LogFileLocationNew"];
                string logFileLocationUpdate = ConfigurationManager.AppSettings["LogFileLocationUpdate"];

                Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.File(logFileLocationNew + "\\ImportServiceLog - " + DateTime.Now.ToString(@"yyyy-MM-dd hh-mm-ss") + ".txt").CreateLogger();

                // Get Unique GUID for this process
                Guid guid = Guid.NewGuid();

                string filePath = Commons.GetCustomizationValue(Constants.CUSTOMIZATION_FILEPATH_NEW);
                if (!Directory.Exists(filePath))
                {
                    throw new Exception ("New XMLs File Path : '" + filePath + "' does not exist");
                }
                
                Log.Debug("-------------------------------------------------------------");
                Log.Debug("Importing XMLs with New Errors");

                // Moving files to temporary folder, so that another instance of the process does not read these files
                Commons.MoveFiles(filePath, filePath + "/" + Constants.FOLDERNAME_PROCESSING + "-" + guid);
                filePath = filePath + "/" + Constants.FOLDERNAME_PROCESSING + "-" + guid;

                // Get all XML files as WorkbenchFile objects from path
                List<WorkbenchFile> newWorkbenchFiles = XMLHelper.GetAllWorkbenchFileObjectsFromPath(filePath);
                Console.WriteLine("New Files : " + newWorkbenchFiles.Count);
                foreach (WorkbenchFile newWorkbenchFile in newWorkbenchFiles)
                {
                    Log.Debug("------------------------------------------------");
                    Log.Debug("Importing file with reference no. - " + newWorkbenchFile.FileName);
                    CreateHelper.CreateWorkbenchFileEntity(newWorkbenchFile);
                }

                Log.Debug("Importing New XMLs complete");
                Log.Debug("-------------------------------------------------------------");
                Directory.Delete(filePath);

                // Adjust File Path and Logger for Update
                Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.File(logFileLocationUpdate + "\\ImportServiceLog - " + DateTime.Now.ToString(@"yyyy-MM-dd hh-mm-ss") + ".txt").CreateLogger();
                filePath = Commons.GetCustomizationValue(Constants.CUSTOMIZATION_FILEPATH_UPDATE);
                if (!Directory.Exists(filePath))
                {
                    throw new Exception("Update XMLs File Path : '" + filePath + "' does not exist");
                }                
                Log.Debug("-------------------------------------------------------------");
                Log.Debug("Importing XMLs with Update Errors");

                // Moving files to temporary folder, so that another instance of the process does not read these files
                Commons.MoveFiles(filePath, filePath + "/" + Constants.FOLDERNAME_PROCESSING + "-" + guid);
                filePath = filePath + "/" + Constants.FOLDERNAME_PROCESSING + "-" + guid;

                List<WorkbenchFile> updateWorkbenchFiles = XMLHelper.GetAllWorkbenchFileObjectsFromPath(filePath);
                Console.WriteLine("Update Files : " + updateWorkbenchFiles.Count);
                foreach (WorkbenchFile updateWorkbenchFile in updateWorkbenchFiles)
                {
                    Log.Debug("------------------------------------------------");
                    Log.Debug("Updating file with reference no. - " + updateWorkbenchFile.FileName);
                    UpdateHelper.UpdateWorkbenchFileEntity(updateWorkbenchFile);
                }

                Log.Debug("Importing Update XMLs complete");
                Log.Debug("-------------------------------------------------------------");
                Directory.Delete(filePath);
            }
            catch (Exception e)
            {
                Log.Error("Error occured while running import service");
                Log.Error("Error is : " + e.Message + ", " + e.InnerException);
            }          

            //Console.WriteLine("Press any key to exit.");
            //Console.ReadKey();
        }
        # endregion        
    }
}
