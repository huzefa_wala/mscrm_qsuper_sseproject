﻿// <company>SMS Management & Technology</company>
// <author>Huzefa Wala</author>
// <date>2/9/2015 1:37:16 PM</date>
// <summary>
//  1. Calculates the Total no. of errors and outstanding errors and sets those values to case
//  2. Set the case priority based on no. of outstanding errors
// </summary>

namespace qsuper.sse.plugins.crm2013
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
      
    public class PluginNumberOfErrors : IPlugin
    {
        # region Variable Declarations
        // Class variables
        private IOrganizationService service;
        private ITracingService tracingService;

        // Constants
        private const string KEY_PRIORITY_MAXFORLOW = "Priority - Maximum for Low";
        private const string KEY_PRIORITY_MAXFORMEDIUM = "Priority - Maximum for Medium";

        private const int OPTIONSETVALUE_PRIORITY_HIGH = 1;
        private const int OPTIONSETVALUE_PRIORITY_MEDIUM = 2;
        private const int OPTIONSETVALUE_PRIORITY_LOW = 3;
        # endregion       

        # region Constructor
        public PluginNumberOfErrors(string config, string secureConfig)
        {
        }
        #endregion

        # region Execute Method
        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances. 
        /// The plug-in's Execute method should be written to be stateless as the constructor 
        /// is not called for every invocation of the plug-in. Also, multiple system threads 
        /// could execute the plug-in at the same time. All per invocation state information 
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        public void Execute(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }


            // Obtain the execution context from the service provider.            
            IPluginExecutionContext context =
                    (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Get a reference to the organization service.
            IOrganizationServiceFactory factory =
                (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            service = factory.CreateOrganizationService(context.UserId);

            // Get a reference to the tracing service.
            tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Variable Declarations
            Decimal noOfErrorsOutstanding;
            int maxForLow;
            int maxForMedium;

            // Get the entity
            Entity entityTarget = (Entity)context.InputParameters["Target"];

            tracingService.Trace("Entity's Name : " + entityTarget.LogicalName);
            // If caller entity is not 'WorkbenchActivity', return
            if (! entityTarget.LogicalName.Equals("new_workbenchactivity"))
            {
                return;
            }
            entityTarget = service.Retrieve(entityTarget.LogicalName, entityTarget.Id, new ColumnSet(true));

            // Get the Parent/related Case
            EntityReference entityRefRelatedCase = entityTarget.GetAttributeValue<EntityReference>("new_case");
            Entity entityCase = service.Retrieve(entityRefRelatedCase.LogicalName, entityRefRelatedCase.Id, new ColumnSet(true));
            tracingService.Trace("Case : " + entityCase.GetAttributeValue<string>("title"));

            // Get all WorkbenchActivities under the related case
            QueryExpression query = new QueryExpression("new_workbenchactivity");
            ConditionExpression condition1 = new ConditionExpression("new_case", ConditionOperator.Equal, entityRefRelatedCase.Id);
            query.Criteria.AddCondition(condition1);
            EntityCollection activityCollection = service.RetrieveMultiple(query);            

            // Set the total as no. of errors in case
            tracingService.Trace("Total no. of errors : " + activityCollection.Entities.Count);
            entityCase.Attributes["new_nooferrors"] = (Decimal) activityCollection.Entities.Count;

            // Get all WorkbenchActivities under related case which are open
            ConditionExpression condition2 = new ConditionExpression("statecode", ConditionOperator.NotEqual, 1);
            query.Criteria.AddCondition(condition2);
            activityCollection = service.RetrieveMultiple(query);            

            // Set the total as no. of errors in case
            tracingService.Trace("Total no. of errors outstanding : " + activityCollection.Entities.Count);
            noOfErrorsOutstanding = (Decimal)activityCollection.Entities.Count;
            entityCase.Attributes["new_nooferrorsoutstanding"] = noOfErrorsOutstanding;

            // Set Priority based on no. of outstanding errors
            maxForLow = Int32.Parse(GetCustomizationValueForKey(KEY_PRIORITY_MAXFORLOW));            

            // If No. of outstanding errors is less than 'Max For Low' setting for priority, set priority as Low
            if (noOfErrorsOutstanding <= maxForLow)
            {
                tracingService.Trace("Setting priority as Low");
                entityCase.Attributes["prioritycode"] = new OptionSetValue(OPTIONSETVALUE_PRIORITY_LOW);
            }
            else
            {
                // Check if priority falls under Medium category requirement
                maxForMedium = Int32.Parse(GetCustomizationValueForKey(KEY_PRIORITY_MAXFORMEDIUM));                

                if (noOfErrorsOutstanding <= maxForMedium)
                {
                    tracingService.Trace("Setting priority as Medium");
                    entityCase.Attributes["prioritycode"] = new OptionSetValue(OPTIONSETVALUE_PRIORITY_MEDIUM);
                }
                else
                {
                    tracingService.Trace("Setting priority as High");
                    entityCase.Attributes["prioritycode"] = new OptionSetValue(OPTIONSETVALUE_PRIORITY_HIGH);
                }
            }

            tracingService.Trace("Updating Case");
            service.Update(entityCase);
            //throw new Exception("Debugging");
        }
        # endregion

        # region GetCustomizationsForKey Method
        // Gets the value of customization based on the key
        private string GetCustomizationValueForKey(string key)
        {
            QueryExpression customizationsQuery = new QueryExpression("new_ssecustomizations");
            ConditionExpression customizationCondition = new ConditionExpression("new_name", ConditionOperator.Equal, key);
            customizationsQuery.Criteria.AddCondition(customizationCondition);

            EntityCollection pcCollection = service.RetrieveMultiple(customizationsQuery);
            tracingService.Trace("Entity retrieved count for key '" + key + "' : " + pcCollection.Entities.Count);

            if (pcCollection.Entities.Count == 0)
            {
                throw new Exception("Unable to find customization key '" + key + "'");
            }

            return service.Retrieve(pcCollection.Entities[0].LogicalName, pcCollection.Entities[0].Id, new ColumnSet(true)).GetAttributeValue<string>("new_value");
        }
        # endregion
     
    }
}